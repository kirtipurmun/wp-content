WebP Express 0.14.22. Conversion triggered using bulk conversion, 2019-09-02 07:01:37

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.3.8
- Server software: Apache/2.4.38 (Debian)

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/elementor/thumbs/google-play-badge-od5vew9v4hb07zetrjvll2j4ekv5c2ulgv1rl5zxu6.png
- destination: [doc-root]/wp-content/uploads/elementor/thumbs/google-play-badge-od5vew9v4hb07zetrjvll2j4ekv5c2ulgv1rl5zxu6.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "all"
- near-lossless: 60
- quality: 85
------------


*Trying: imagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/elementor/thumbs/google-play-badge-od5vew9v4hb07zetrjvll2j4ekv5c2ulgv1rl5zxu6.png
- destination: [doc-root]/wp-content/uploads/elementor/thumbs/google-play-badge-od5vew9v4hb07zetrjvll2j4ekv5c2ulgv1rl5zxu6.webp
- alpha-quality: 85
- encoding: "auto"
- log-call-arguments: true
- metadata: "all"
- quality: 85

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- low-memory: false
- max-quality: 85
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Quality: 85. 
Reduction: 62% (went from 4984 bytes to 1880 bytes)

Converting to lossless
Reduction: 53% (went from 4984 bytes to 2330 bytes)

Picking lossy
imagick succeeded :)

Converted image in 27 ms, reducing file size with 62% (went from 4984 bytes to 1880 bytes)
