WebP Express 0.14.22. Conversion triggered using bulk conversion, 2019-08-27 09:45:17

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.3.8
- Server software: Apache/2.4.38 (Debian)

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/elementor/thumbs/google-play-badge-ocvq7zwft793zbh4f39jyp1wpfyxa10c3u58dtu7r8.png
- destination: [doc-root]/wp-content/uploads/elementor/thumbs/google-play-badge-ocvq7zwft793zbh4f39jyp1wpfyxa10c3u58dtu7r8.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "all"
- near-lossless: 60
- quality: 85
------------


*Trying: imagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/elementor/thumbs/google-play-badge-ocvq7zwft793zbh4f39jyp1wpfyxa10c3u58dtu7r8.png
- destination: [doc-root]/wp-content/uploads/elementor/thumbs/google-play-badge-ocvq7zwft793zbh4f39jyp1wpfyxa10c3u58dtu7r8.webp
- alpha-quality: 85
- encoding: "auto"
- log-call-arguments: true
- metadata: "all"
- quality: 85

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- low-memory: false
- max-quality: 85
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Quality: 85. 
Reduction: 61% (went from 4688 bytes to 1828 bytes)

Converting to lossless
Reduction: 49% (went from 4688 bytes to 2374 bytes)

Picking lossy
imagick succeeded :)

Converted image in 28 ms, reducing file size with 61% (went from 4688 bytes to 1828 bytes)
