WebP Express 0.14.22. Conversion triggered using bulk conversion, 2019-08-22 12:13:11

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.3.8
- Server software: Apache/2.4.38 (Debian)

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/elementor/thumbs/VK4.2.3.1-O1-P1000635_LR-obacd9mcz15og9171fcp5lcj92dhb1ixe4fv32uedo.jpg
- destination: [doc-root]/wp-content/uploads/elementor/thumbs/VK4.2.3.1-O1-P1000635_LR-obacd9mcz15og9171fcp5lcj92dhb1ixe4fv32uedo.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "all"
- near-lossless: 60
- quality: "auto"
------------


*Trying: imagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/elementor/thumbs/VK4.2.3.1-O1-P1000635_LR-obacd9mcz15og9171fcp5lcj92dhb1ixe4fv32uedo.jpg
- destination: [doc-root]/wp-content/uploads/elementor/thumbs/VK4.2.3.1-O1-P1000635_LR-obacd9mcz15og9171fcp5lcj92dhb1ixe4fv32uedo.webp
- default-quality: 70
- encoding: "auto"
- log-call-arguments: true
- max-quality: 80
- metadata: "all"
- quality: "auto"

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- low-memory: false
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Quality of source is 82. This is higher than max-quality, so using max-quality instead (80)
Reduction: 28% (went from 95 kb to 69 kb)

Converting to lossless
Reduction: -331% (went from 95 kb to 411 kb)

Picking lossy
imagick succeeded :)

Converted image in 683 ms, reducing file size with 28% (went from 95 kb to 69 kb)
