WebP Express 0.14.22. Conversion triggered using bulk conversion, 2019-09-05 05:04:17

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.3.8
- Server software: Apache/2.4.38 (Debian)

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/elementor/thumbs/kirtipur-hiking-trails-odb2dhmc9y1rueyvtrkysy8wdi33cw2wck34pgij48.jpg
- destination: [doc-root]/wp-content/uploads/elementor/thumbs/kirtipur-hiking-trails-odb2dhmc9y1rueyvtrkysy8wdi33cw2wck34pgij48.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "all"
- near-lossless: 60
- quality: "auto"
------------


*Trying: imagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/elementor/thumbs/kirtipur-hiking-trails-odb2dhmc9y1rueyvtrkysy8wdi33cw2wck34pgij48.jpg
- destination: [doc-root]/wp-content/uploads/elementor/thumbs/kirtipur-hiking-trails-odb2dhmc9y1rueyvtrkysy8wdi33cw2wck34pgij48.webp
- default-quality: 70
- encoding: "auto"
- log-call-arguments: true
- max-quality: 80
- metadata: "all"
- quality: "auto"

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- low-memory: false
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Quality of source is 85. This is higher than max-quality, so using max-quality instead (80)
Reduction: 43% (went from 27 kb to 15 kb)

Converting to lossless
Reduction: -230% (went from 27 kb to 88 kb)

Picking lossy
imagick succeeded :)

Converted image in 386 ms, reducing file size with 43% (went from 27 kb to 15 kb)
