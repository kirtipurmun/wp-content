WebP Express 0.14.22. Conversion triggered using bulk conversion, 2019-08-22 12:13:13

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.3.8
- Server software: Apache/2.4.38 (Debian)

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/elementor/thumbs/kirtipur-hiking-white-house-ocaw5vvzix6p4e49ql3xcir2k6cvrhpq5avnzylqq0.jpg
- destination: [doc-root]/wp-content/uploads/elementor/thumbs/kirtipur-hiking-white-house-ocaw5vvzix6p4e49ql3xcir2k6cvrhpq5avnzylqq0.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "all"
- near-lossless: 60
- quality: "auto"
------------


*Trying: imagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/elementor/thumbs/kirtipur-hiking-white-house-ocaw5vvzix6p4e49ql3xcir2k6cvrhpq5avnzylqq0.jpg
- destination: [doc-root]/wp-content/uploads/elementor/thumbs/kirtipur-hiking-white-house-ocaw5vvzix6p4e49ql3xcir2k6cvrhpq5avnzylqq0.webp
- default-quality: 70
- encoding: "auto"
- log-call-arguments: true
- max-quality: 80
- metadata: "all"
- quality: "auto"

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- low-memory: false
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Quality of source is 82. This is higher than max-quality, so using max-quality instead (80)
Reduction: 27% (went from 59 kb to 43 kb)

Converting to lossless
Reduction: -354% (went from 59 kb to 267 kb)

Picking lossy
imagick succeeded :)

Converted image in 439 ms, reducing file size with 27% (went from 59 kb to 43 kb)
