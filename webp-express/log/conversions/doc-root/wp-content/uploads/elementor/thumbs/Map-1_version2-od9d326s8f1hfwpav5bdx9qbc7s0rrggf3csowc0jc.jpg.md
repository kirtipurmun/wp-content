WebP Express 0.14.22. Conversion triggered using bulk conversion, 2019-09-04 15:53:29

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.3.8
- Server software: Apache/2.4.38 (Debian)

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/elementor/thumbs/Map-1_version2-od9d326s8f1hfwpav5bdx9qbc7s0rrggf3csowc0jc.jpg
- destination: [doc-root]/wp-content/uploads/elementor/thumbs/Map-1_version2-od9d326s8f1hfwpav5bdx9qbc7s0rrggf3csowc0jc.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "all"
- near-lossless: 60
- quality: "auto"
------------


*Trying: imagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/elementor/thumbs/Map-1_version2-od9d326s8f1hfwpav5bdx9qbc7s0rrggf3csowc0jc.jpg
- destination: [doc-root]/wp-content/uploads/elementor/thumbs/Map-1_version2-od9d326s8f1hfwpav5bdx9qbc7s0rrggf3csowc0jc.webp
- default-quality: 70
- encoding: "auto"
- log-call-arguments: true
- max-quality: 80
- metadata: "all"
- quality: "auto"

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- low-memory: false
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Quality of source is 85. This is higher than max-quality, so using max-quality instead (80)
Reduction: 50% (went from 8756 bytes to 4404 bytes)

Converting to lossless
Reduction: -265% (went from 8756 bytes to 31916 bytes)

Picking lossy
imagick succeeded :)

Converted image in 170 ms, reducing file size with 50% (went from 8756 bytes to 4404 bytes)
