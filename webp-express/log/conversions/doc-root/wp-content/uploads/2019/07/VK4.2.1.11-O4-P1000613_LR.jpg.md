WebP Express 0.14.22. Conversion triggered using bulk conversion, 2019-08-22 12:07:11

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.3.8
- Server software: Apache/2.4.38 (Debian)

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2019/07/VK4.2.1.11-O4-P1000613_LR.jpg
- destination: [doc-root]/wp-content/uploads/2019/07/VK4.2.1.11-O4-P1000613_LR.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "all"
- near-lossless: 60
- quality: "auto"
------------


*Trying: imagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2019/07/VK4.2.1.11-O4-P1000613_LR.jpg
- destination: [doc-root]/wp-content/uploads/2019/07/VK4.2.1.11-O4-P1000613_LR.webp
- default-quality: 70
- encoding: "auto"
- log-call-arguments: true
- max-quality: 80
- metadata: "all"
- quality: "auto"

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- low-memory: false
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Quality set to same as source: 80
Reduction: 44% (went from 1018 kb to 574 kb)

Converting to lossless
Reduction: -449% (went from 1018 kb to 5593 kb)


*Warning: filesize(): stat failed for [doc-root]/wp-content/uploads/2019/07/VK4.2.1.11-O4-P1000613_LR.webp.lossy.webp in [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/ConverterTraits/EncodingAutoTrait.php, line 70, PHP 7.3.8 (Linux)* 


*Warning: filesize(): stat failed for [doc-root]/wp-content/uploads/2019/07/VK4.2.1.11-O4-P1000613_LR.webp.lossy.webp in [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/ConverterTraits/EncodingAutoTrait.php, line 70, PHP 7.3.8 (Linux)* 

Picking lossy

*Warning: rename([doc-root]/wp-content/uploads/2019/07/VK4.2.1.11-O4-P1000613_LR.webp.lossy.webp,[doc-root]/wp-content/uploads/2019/07/VK4.2.1.11-O4-P1000613_LR.webp): No such file or directory in [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/ConverterTraits/EncodingAutoTrait.php, line 73, PHP 7.3.8 (Linux)* 


*Warning: rename([doc-root]/wp-content/uploads/2019/07/VK4.2.1.11-O4-P1000613_LR.webp.lossy.webp,[doc-root]/wp-content/uploads/2019/07/VK4.2.1.11-O4-P1000613_LR.webp): No such file or directory in [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/ConverterTraits/EncodingAutoTrait.php, line 73, PHP 7.3.8 (Linux)* 

imagick succeeded :)

Converted image in 15556 ms, reducing file size with 44% (went from 1018 kb to 574 kb)
