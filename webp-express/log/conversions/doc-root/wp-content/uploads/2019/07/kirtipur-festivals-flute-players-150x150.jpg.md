WebP Express 0.14.22. Conversion triggered using bulk conversion, 2019-08-22 11:51:56

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.3.8
- Server software: Apache/2.4.38 (Debian)

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2019/07/kirtipur-festivals-flute-players-150x150.jpg
- destination: [doc-root]/wp-content/uploads/2019/07/kirtipur-festivals-flute-players-150x150.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "all"
- near-lossless: 60
- quality: "auto"
------------


*Trying: imagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2019/07/kirtipur-festivals-flute-players-150x150.jpg
- destination: [doc-root]/wp-content/uploads/2019/07/kirtipur-festivals-flute-players-150x150.webp
- default-quality: 70
- encoding: "auto"
- log-call-arguments: true
- max-quality: 80
- metadata: "all"
- quality: "auto"

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- low-memory: false
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Quality set to same as source: 80
Reduction: 24% (went from 7822 bytes to 5934 bytes)

Converting to lossless
Reduction: -279% (went from 7822 bytes to 29674 bytes)

Picking lossy
imagick succeeded :)

Converted image in 157 ms, reducing file size with 24% (went from 7822 bytes to 5934 bytes)
