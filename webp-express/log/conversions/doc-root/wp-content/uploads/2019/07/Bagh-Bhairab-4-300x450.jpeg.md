WebP Express 0.14.22. Conversion triggered using bulk conversion, 2019-08-22 12:10:06

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.3.8
- Server software: Apache/2.4.38 (Debian)

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2019/07/Bagh-Bhairab-4-300x450.jpeg
- destination: [doc-root]/wp-content/uploads/2019/07/Bagh-Bhairab-4-300x450.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "all"
- near-lossless: 60
- quality: "auto"
------------


*Trying: imagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2019/07/Bagh-Bhairab-4-300x450.jpeg
- destination: [doc-root]/wp-content/uploads/2019/07/Bagh-Bhairab-4-300x450.webp
- default-quality: 70
- encoding: "auto"
- log-call-arguments: true
- max-quality: 80
- metadata: "all"
- quality: "auto"

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- low-memory: false
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Quality of source is 100. This is higher than max-quality, so using max-quality instead (80)
Reduction: 75% (went from 161 kb to 40 kb)

Converting to lossless
Reduction: -15% (went from 161 kb to 185 kb)

Picking lossy
imagick succeeded :)

Converted image in 392 ms, reducing file size with 75% (went from 161 kb to 40 kb)
