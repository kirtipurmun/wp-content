WebP Express 0.14.22. Conversion triggered with the conversion script (wod/webp-on-demand.php), 2019-08-22 11:00:54

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.3.8
- Server software: Apache/2.4.38 (Debian)

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2019/07/chobhar-gorge-bagmati-cliffs-150x150.jpeg
- destination: [doc-root]/wp-content/uploads/2019/07/chobhar-gorge-bagmati-cliffs-150x150.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "all"
- near-lossless: 60
- quality: "auto"
------------


*Trying: imagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2019/07/chobhar-gorge-bagmati-cliffs-150x150.jpeg
- destination: [doc-root]/wp-content/uploads/2019/07/chobhar-gorge-bagmati-cliffs-150x150.webp
- default-quality: 70
- encoding: "auto"
- log-call-arguments: true
- max-quality: 80
- metadata: "all"
- quality: "auto"

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- low-memory: false
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Quality set to same as source: 80
Reduction: 15% (went from 8089 bytes to 6868 bytes)

Converting to lossless
Reduction: -285% (went from 8089 bytes to 31152 bytes)

Picking lossy
imagick succeeded :)

Converted image in 143 ms, reducing file size with 15% (went from 8089 bytes to 6868 bytes)
