WebP Express 0.14.22. Conversion triggered with the conversion script (wod/webp-on-demand.php), 2019-08-22 10:52:31

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.3.8
- Server software: Apache/2.4.38 (Debian)

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/plugins/leaflet-maps-marker/inc/img/icon-menu-list-white.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/plugins/leaflet-maps-marker/inc/img/icon-menu-list-white.png.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "all"
- near-lossless: 60
- quality: 85
------------


*Trying: imagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/plugins/leaflet-maps-marker/inc/img/icon-menu-list-white.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/plugins/leaflet-maps-marker/inc/img/icon-menu-list-white.png.webp
- alpha-quality: 85
- encoding: "auto"
- log-call-arguments: true
- metadata: "all"
- quality: 85

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- low-memory: false
- max-quality: 85
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Quality: 85. 
Reduction: 6% (went from 102 bytes to 96 bytes)

Converting to lossless
Reduction: 47% (went from 102 bytes to 54 bytes)

Picking lossless
imagick succeeded :)

Converted image in 104 ms, reducing file size with 47% (went from 102 bytes to 54 bytes)
