WebP Express 0.14.22. Conversion triggered using bulk conversion, 2019-08-22 12:14:13

*WebP Convert 2.1.4*  ignited.
- PHP version: 7.3.8
- Server software: Apache/2.4.38 (Debian)

Stack converter ignited
Destination folder does not exist. Creating folder: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/plugins/auxin-elements/admin/assets/images/welcome

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/plugins/auxin-elements/admin/assets/images/welcome/waspthemes-yellow-pencil-plugin.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/plugins/auxin-elements/admin/assets/images/welcome/waspthemes-yellow-pencil-plugin.png.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "all"
- near-lossless: 60
- quality: 85
------------


*Trying: imagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/plugins/auxin-elements/admin/assets/images/welcome/waspthemes-yellow-pencil-plugin.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/plugins/auxin-elements/admin/assets/images/welcome/waspthemes-yellow-pencil-plugin.png.webp
- alpha-quality: 85
- encoding: "auto"
- log-call-arguments: true
- metadata: "all"
- quality: 85

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- low-memory: false
- max-quality: 85
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Quality: 85. 
Reduction: -40% (went from 1265 bytes to 1766 bytes)

Converting to lossless
Reduction: 23% (went from 1265 bytes to 976 bytes)

Picking lossless
imagick succeeded :)

Converted image in 14 ms, reducing file size with 23% (went from 1265 bytes to 976 bytes)
