<?php 
return array(
        
        array(
            'key'        => 'cooked_compatibility_for_schema',
            'name'       => 'Cooked Compatibility for Schema',
            'path'       => 'cooked-compatibility-for-schema/cooked-compatibility-for-schema.php',                        
        ),
        array(
            'key'        => 'woocommerce_compatibility_for_schema',
            'name'       => 'WooCommerce Compatibility for Schema',
            'path'       => 'woocommerce-compatibility-for-schema/woocommerce-compatibility-for-schema.php',                        
        ),
        array(
            'key'        => 'reviews_for_schema',
            'name'       => 'Reviews for Schema',
            'path'       => 'reviews-for-schema/reviews-for-schema.php',                        
        )
                        
    );